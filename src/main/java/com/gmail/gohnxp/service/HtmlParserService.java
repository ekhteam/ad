package com.gmail.gohnxp.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.http.client.utils.URIBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.gmail.gohnxp.domain.AdType;
import com.gmail.gohnxp.domain.SearchResultRecord;
import com.gmail.gohnxp.domain.SearchSystem;

@Service
public class HtmlParserService {

	@Value("${path.phrasesFile}")
	private String pathPhrasesFile;
	@Value("${path.destCsvFile}")
	private String pathDestCsvFile;
	@Value("${path.chromeDriver}")
	private String pathChromeDriver;
	@Value("${path.geckoDriver}")
	private String pathGeckoDriver;
	@Value("${path.torProfileDefault}")
	private String pathTorProfileDefault;
	@Value("${path.torExec}")
	private String pathTorExec;

	private static Logger LOGGER = LoggerFactory.getLogger(HtmlParserService.class);

	private final static int PARSED_SITES_COUNT_FOR_FLUSH = 20;

	FirefoxBinary tor = null;
	WebDriver driver = null;

	private void openTorBrowser() {
		File torProfileFile = new File(pathTorProfileDefault);
		FirefoxProfile torProfile = new FirefoxProfile(torProfileFile);
		tor = new FirefoxBinary(new File(pathTorExec));
		torProfile.setPreference("webdriver.load.strategy", "unstable");

		try {
			tor.startProfile(torProfile, torProfileFile, "");
		} catch (IOException e) {
			e.printStackTrace();
		}

		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("network.proxy.type", 1);
		profile.setPreference("network.proxy.socks", "127.0.0.1");
		profile.setPreference("network.proxy.socks_port", 9150);
		profile.setPreference("browser.privatebrowsing.autostart", true);
		profile.setPreference("network.cookie.cookieBehavior", 2);
		profile.setPreference("permissions.default.stylesheet", 2);
		profile.setPreference("permissions.default.image", 2);
//		profile.setPreference("javascript.enabled", false);
		profile.setPreference("dom.ipc.plugins.enabled.libflashplayer.so", false);
		profile.setPreference("browser.migration.version", 9001);
		driver = new FirefoxDriver(profile);
	}

	private void closeTorBrowser() {
		driver.close();
		tor.quit();
	}

	public boolean parseHtml(Date groupDate) throws IOException {
		List<SearchResultRecord> parsedData = new ArrayList<>();

		List<String> phrases = FileUtils.readLines(new File(pathPhrasesFile), "UTF-8");

		// System.setProperty("webdriver.chrome.driver", pathChromeDriver);
		// WebDriver driver = new ChromeDriver();
		openTorBrowser();

		int currentCount = 0;

		// Random randomizer = new Random();

		for (int i = 0; i < phrases.size(); i++) {
			String phrase = phrases.get(i);// randomizer.nextInt(phrases.size()));
			for (SearchSystem searchSystem : SearchSystem.values()) {
				LOGGER.debug("Parse in '" + searchSystem + "' for word '" + phrase + "'");

				if (searchSystem == SearchSystem.GOOGLE) {
					parsedData.addAll(getDataFromGoogle(phrase, groupDate.getTime(), groupDate));
				} else if (searchSystem == SearchSystem.YANDEX) {
					parsedData.addAll(getDataFromYandex(phrase, groupDate.getTime(), groupDate));
				}
			}

			currentCount++;

			if (PARSED_SITES_COUNT_FOR_FLUSH == currentCount) {
				currentCount = 0;
				saveParsedData(parsedData);
				parsedData.clear();
			}
		}

		saveParsedData(parsedData);
		closeTorBrowser();

		return true;
	}

	private static final String COMMA_DELIMITER = "<$$$>";
	private static final String NEW_LINE_SEPARATOR = "\n";

	private static final String CSV_FILE_HEADER = ("adType,groupId,groupDate,phrase,searchSystem,"
			+ "order,url,title,snippet").replace(",", COMMA_DELIMITER);

	public FileWriter prepareFileWriter(Date groupDate) throws IOException {

		FileWriter fileWriter = new FileWriter(
				pathDestCsvFile + "___" + new SimpleDateFormat("yyyy-MM-dd_hh_mm").format(groupDate) + ".csv", true);

		fileWriter.append(CSV_FILE_HEADER.toString());
		fileWriter.append(NEW_LINE_SEPARATOR);

		return fileWriter;
	}

	public void saveParsedData(List<SearchResultRecord> parsedData) throws IOException {
		FileWriter fileWriter = null;
		try {
			fileWriter = prepareFileWriter(parsedData.get(0).getGroupDate());
			for (SearchResultRecord record : parsedData) {
				fileWriter.append(String.valueOf(record.getAdType()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(record.getGroupId()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(new SimpleDateFormat("yyyy-MM-dd_hh_mm").format(record.getGroupDate()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(record.getPhrase()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(record.getSearchSystem()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(record.getOrder()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(record.getUrl()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(record.getTitle()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(record.getSnippet()));
				fileWriter.append(NEW_LINE_SEPARATOR);
			}
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {

			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}
	}

	private List<SearchResultRecord> tryParseGoogleByAlg1(String phrase, long groupId, Date groupDate) {
		List<SearchResultRecord> result = null;

		boolean isNeedToLoop = true;

		while (isNeedToLoop) {
			result = new ArrayList<>();

			try {
				String searchQuery = "https://www.google.com/search?num=20&q=" + URLEncoder.encode(phrase, "UTF-8");

				driver.get(searchQuery);

				AdType adType = AdType.AD;
				List<WebElement> adBlocks = driver.findElements(By.cssSelector("#tads > ol > li"));
				final int adCount = adBlocks.size();

				if (adCount == 0)
					return null;

				for (int i = 0; i < adBlocks.size(); i++) {
					WebElement adBlock = adBlocks.get(i);

					WebElement titleHref = adBlock.findElement(By.cssSelector("h3 > a:nth-child(2)"));
					String title = titleHref.getText();

					String href = adBlock.findElement(By.cssSelector("cite")).getText();

					WebElement snipperElement = adBlock.findElement(By.cssSelector("div.ads-creative"));
					String snippet = snipperElement.getText();

					SearchResultRecord record = new SearchResultRecord(groupId, groupDate, i, href, title, snippet,
							adType, SearchSystem.GOOGLE, phrase);
					result.add(record);
				}

				adType = AdType.NATURAL;
				final List<WebElement> naturalBlocks = driver.findElements(By.cssSelector("#rso > div div.rc"));
				for (int i = 0; i < naturalBlocks.size(); i++) {
					WebElement naturalBlock = naturalBlocks.get(i);

					WebElement titleHref = naturalBlock.findElement(By.cssSelector("h3 > a"));
					String title = titleHref.getText();

					String href = naturalBlock.findElement(By.cssSelector("cite")).getText();

					List<WebElement> snipperElements = naturalBlock.findElements(By.cssSelector("span.st"));
					String snippet = snipperElements.size() != 0 ? snipperElements.get(0).getText() : "";

					SearchResultRecord record = new SearchResultRecord(groupId, groupDate, i + adCount, href, title,
							snippet, adType, SearchSystem.GOOGLE, phrase);
					result.add(record);
				}
				isNeedToLoop = false;
			} catch (Exception e) {
				LOGGER.error("Can't parse for phrase '" + phrase + "'", e);
			}
		}
		return result;
	}

	private List<SearchResultRecord> tryParseGoogleByAlg2(String phrase, long groupId, Date groupDate) {
		List<SearchResultRecord> result = null;

		boolean isNeedToLoop = true;

		while (isNeedToLoop) {
			result = new ArrayList<>();

			try {
				String searchQuery = "https://www.google.com/search?num=20&q=" + URLEncoder.encode(phrase, "UTF-8");

				driver.get(searchQuery);
				
				AdType adType = AdType.AD;
				List<WebElement> adBlocks = driver.findElements(By.cssSelector("li.ads-ad"));
				int adCount = adBlocks.size();

				for (int i = 0; i < adBlocks.size(); i++) {
					WebElement adBlock = adBlocks.get(i);

					WebElement titleHref = adBlock.findElement(By.cssSelector("h3 > a:nth-child(2)"));
					String title = titleHref.getText();

					String href = adBlock.findElement(By.cssSelector("cite")).getText();

					WebElement snipperElement = adBlock.findElement(By.cssSelector("div.ads-creative"));
					String snippet = snipperElement.getText();

					SearchResultRecord record = new SearchResultRecord(groupId, groupDate, i, href, title, snippet,
							adType, SearchSystem.GOOGLE, phrase);
					result.add(record);
				}

				adType = AdType.NATURAL;
				final List<WebElement> naturalBlocks = driver.findElements(By.cssSelector(".g:not([id='imagebox_bigimages'])"));
				adCount = naturalBlocks.size();

				if (adCount == 0)
					return null;

				for (int i = 0; i < naturalBlocks.size(); i++) {
					WebElement naturalBlock = naturalBlocks.get(i);

					WebElement titleHref = naturalBlock.findElement(By.cssSelector("h3 a"));
					String title = titleHref.getText();

					String href = naturalBlock.findElement(By.cssSelector("cite")).getText();

					WebElement snipperElement = naturalBlock.findElement(By.cssSelector("span.st"));
					String snippet = snipperElement.getText();

					SearchResultRecord record = new SearchResultRecord(groupId, groupDate, i + adCount, href, title,
							snippet, adType, SearchSystem.GOOGLE, phrase);
					result.add(record);
				}
				isNeedToLoop = false;
			} catch (Exception e) {
				LOGGER.error("Can't parse for phrase '" + phrase + "'", e);
			}
		}
		return result;
	}

	private List<SearchResultRecord> getDataFromGoogle(String phrase, long groupId, Date groupDate) {
		List<SearchResultRecord> result = new ArrayList<>();

		while (true) {

			result = tryParseGoogleByAlg1(phrase, groupId, groupDate);

			if (result == null) {
				result = tryParseGoogleByAlg2(phrase, groupId, groupDate);

				if (result == null) {
					/*
					 * try { Thread.sleep(10000); } catch (InterruptedException
					 * e) { LOGGER.error("Error", e); }
					 */
					closeTorBrowser();
					openTorBrowser();
					continue;
				}
			}

			break;
		}

		return result;
	}

	private List<SearchResultRecord> getDataFromYandex(String phrase, long groupId, Date groupDate) {

		List<SearchResultRecord> result = null;

		boolean isNeedToLoop = true;

		while (isNeedToLoop) {
			result = new ArrayList<>();
			try {
				String searchQuery = "https://yandex.ru/search/?lr=213&text=" + URLEncoder.encode(phrase, "UTF-8");

				driver.get(searchQuery);

				String blockSelector = "body > div.main.main_right-panel-hidden_yes.main_distro-footer_yes.serp"
						+ ".i-bem.main_js_inited.serp_js_inited > div.main__center > div.main__content > "
						+ "div.content.i-bem.content_js_inited > div.content__left > div.serp-list div > "
						+ "div.serp-item[data-counter-block-id]:not(.z-images):not(.show-feedback__container)"
						+ ":not(.z-market):not(.z-companies)";
				List<WebElement> allBlocks = driver.findElements(By.cssSelector(blockSelector));

				for (int i = 0; i < allBlocks.size(); ++i) {
					WebElement adBlock = allBlocks.get(i);
					int blockId = Integer.valueOf(adBlock.getAttribute("data-counter-block-id"));

					AdType adType = adBlock.findElements(By.cssSelector(".serp-adv-item__label")).size() != 0
							? AdType.AD : AdType.NATURAL;

					String title = adBlock.findElement(By.cssSelector("h2")).getText();

					String href = adBlock.findElement(By.cssSelector(".serp-item__greenurl " + "> span.serp-url__item"))
							.getText();

					String snippet = null;

					List<WebElement> snippetElements = driver.findElements(By.cssSelector(
							blockSelector + "[data-counter-block-id=\"" + blockId + "\"] " + " .serp-item__text"));

					if (snippetElements.size() != 0) {
						snippet = snippetElements.get(0).getText();
					} else {
						snippetElements = driver.findElements(By.cssSelector(
								blockSelector + "[data-counter-block-id=\"" + blockId + "\"] " + " .organic__text"));

						if (snippetElements.size() != 0) {
							snippet = snippetElements.get(0).getText();
						} else {
							snippetElements = driver.findElements(By.cssSelector(blockSelector
									+ "[data-counter-block-id=\"" + blockId + "\"] " + " .serp-item__snp-list"));
							System.out.println("blockId == " + blockId + " ; phrase == " + phrase);
							snippet = snippetElements.get(0).getText();

						}
					}

					SearchResultRecord record = new SearchResultRecord(groupId, groupDate, i, href, title, snippet,
							adType, SearchSystem.YANDEX, phrase);
					result.add(record);
				}
				isNeedToLoop = false;
			} catch (Exception e) {
				LOGGER.error("Can't parse for phrase '" + phrase + "'", e);
			}
		}

		return result;
	}
}
