package com.gmail.gohnxp.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gmail.gohnxp.domain.SearchResultRecord;
import com.gmail.gohnxp.service.HtmlParserService;

@Controller
@RequestMapping("/api/html_parser")
public class HtmlParserController {

	private Logger logger = LoggerFactory.getLogger(HtmlParserController.class);

	@Autowired
	HtmlParserService htmlParserService;

	@RequestMapping(value = "parse", method = { RequestMethod.GET }, produces = { "application/json" })
	public @ResponseBody ResponseEntity<Boolean> parse() {
		boolean result = false;

		Date groupDate = new Date(GregorianCalendar.getInstance().getTimeInMillis());

		try {
			htmlParserService.parseHtml(groupDate);
		} catch (IOException ioe) {
			logger.error("Error while searching for report!", ioe);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
