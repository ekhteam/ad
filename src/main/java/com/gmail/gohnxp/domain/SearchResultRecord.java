package com.gmail.gohnxp.domain;

import java.io.Serializable;
import java.sql.Date;

public class SearchResultRecord implements Serializable  {
	
	private static final long serialVersionUID = 1L;

	private long groupId;
	private Date groupDate;

	private int order;
	private String url;
	private String title;
	private String snippet;
	
	private AdType adType;
	private SearchSystem searchSystem;
	private String phrase;

	public SearchResultRecord(long groupId, Date groupDate, int order, String url, String title, String snippet,
			AdType adType, SearchSystem searchSystem, String word) {
		super();
		this.groupId = groupId;
		this.groupDate = groupDate;
		this.order = order;
		this.url = url;
		this.title = title;
		this.snippet = snippet;
		this.adType = adType;
		this.searchSystem = searchSystem;
		this.phrase = word;
	}

	public SearchResultRecord() {
		super();
	}

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public Date getGroupDate() {
		return groupDate;
	}

	public void setGroupDate(Date groupDate) {
		this.groupDate = groupDate;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}

	public AdType getAdType() {
		return adType;
	}

	public void setAdType(AdType adType) {
		this.adType = adType;
	}

	public SearchSystem getSearchSystem() {
		return searchSystem;
	}

	public void setSearchSystem(SearchSystem searchSystem) {
		this.searchSystem = searchSystem;
	}

	public String getPhrase() {
		return phrase;
	}

	public void setPhrase(String phrase) {
		this.phrase = phrase;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
